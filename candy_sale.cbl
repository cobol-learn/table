       IDENTIFICATION DIVISION. 
       PROGRAM-ID. CANNY-SALE.
       AUTHOR. PAKAWAT.
       
       
       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT CANNY_SALE_FILE  ASSIGN TO "candy_sale.dat"
              ORGANIZATION   IS LINE  SEQUENTIAL.

       

       DATA DIVISION.
       FILE SECTION. 
       FD  CANNY_SALE_FILE.
       01  CANNY_SALE_REC.
           88 END-OF-FILE VALUE HIGH-VALUES.
           05 BRANCH-ID   PIC 9(7).
           05 STATE-NUM   PIC   99.
           05 CANDY-SALES PIC   9(7)V99.


       WORKING-STORAGE SECTION. 
       01  STATE-SALE-TABLE.
      *    มีทั้งหมด 50 state
           05 STATE-SALES-TOTAL PIC   9(8)V99  OCCURS 50   TIMES.
      
      *    **COUNT
       01  STATE-IDX   PIC   99.
       01  PRN-STATE-SALE PIC   $$$,$$$,$$9.99.
       01  PRN-STATE-IDX PIC   ZZ.


       PROCEDURE DIVISION.
           MOVE  ZEROS TO STATE-SALE-TABLE 
           OPEN  INPUT CANNY_SALE_FILE 
           
           PERFORM READ-RECORD-FILE

           PERFORM UNTIL END-OF-FILE
               ADD   CANDY-SALES TO STATE-SALES-TOTAL(STATE-NUM)
               PERFORM READ-RECORD-FILE
           END-PERFORM

      *    PRN
           PERFORM  PRN-SALE-BY-STATE

           CLOSE CANNY_SALE_FILE  
           GOBACK 
       .


       PRN-SALE-BY-STATE.
           DISPLAY "   YoureCandyShop Sales by State"
           DISPLAY "   -----------------------------"
           PERFORM VARYING STATE-IDX FROM 1 BY 1  UNTIL STATE-IDX 
              >=50
              
              MOVE STATE-SALES-TOTAL(STATE-IDX) TO PRN-STATE-SALE
              MOVE  STATE-IDX TO PRN-STATE-IDX
              DISPLAY "State ", PRN-STATE-IDX 
                      " sales total is "PRN-STATE-SALE
           END-PERFORM
           
           EXIT 
           .

       READ-RECORD-FILE.
           READ CANNY_SALE_FILE
           AT END SET END-OF-FILE  TO TRUE
           END-READ 
           EXIT 
           .