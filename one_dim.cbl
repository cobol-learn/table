       IDENTIFICATION DIVISION. 
       PROGRAM-ID. ONE-DIM.
       AUTHOR. PAKAWAT.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  WS-TABLE.
           05 WS-A  PIC   X(10) VALUE '**BUU**'  OCCURS 5 TIMES.
       01  WS-IDX   PIC   9(2).

       PROCEDURE DIVISION .
      *    **BUU**   **BUU**   **BUU**   **BUU**   **BUU**   
           DISPLAY WS-TABLE 
      *    **BUU**
           DISPLAY WS-A(1)
           DISPLAY WS-A(2)
           DISPLAY WS-A(3)
           DISPLAY WS-A(4)
           DISPLAY WS-A(5)
           DISPLAY "--------------------"
      *    PERFORM VARYING
           PERFORM VARYING   WS-IDX FROM 1  BY 1  UNTIL WS-IDX>5
              DISPLAY  WS-A(WS-IDX)
           END-PERFORM
           DISPLAY "--------------------"
      *    PERFORM OUTLINE
            PERFORM DO-DISPLAY
             VARYING   WS-IDX FROM 1  BY 1  UNTIL WS-IDX>5
           GOBACK 
       .

       DO-DISPLAY.
           DISPLAY  WS-A(WS-IDX)          
           EXIT 
       .

