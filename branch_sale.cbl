       IDENTIFICATION DIVISION. 
       PROGRAM-ID. BRANCH-SALE.
       AUTHOR. PAKAWAT.
       
       
       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT BRANCH_SALE_FILE  ASSIGN TO "branch_sales.dat"
              ORGANIZATION   IS LINE  SEQUENTIAL.

       

       DATA DIVISION.
       FILE SECTION. 
       FD  BRANCH_SALE_FILE.
       01  BRANCH_SALE_REC.
           88 END-OF-FILE VALUE HIGH-VALUES.
           05 BRANCH-ID   PIC 9(7).
           05 STATE-NUM   PIC   99.
           05 CANDY-SALES PIC   9(7)V99.


       WORKING-STORAGE SECTION. 
       01  STATE-SALE-TABLE.
      *    มีทั้งหมด 50 state , 1 state มีหลาย branch
           05 STATE-TOTAL OCCURS 50   TIMES.
      *    ผลรวมยอดขายแต่ละ state
              10 STATE-SALES-TOTAL pic   9(8)V99.
              10 STATE-BRANCH-COUNT pic   9(5).

      *    **COUNT
       01  STATE-IDX   PIC   99.
       
      *    PRINT-DETAIL
       01  PRINT-DETAIL-LINE.
           05 PRN-SATTE-NUM PIC ZZ.
           05 PRN-BRANCH-COUNT PIC B(3)ZZ,ZZ9.
           05 PRN-STATE-SALE PIC   B(5)$$$,$$$,$$9.99.
           05 PRN-AVERAGE-SALES PIC  BB$$$,$$$,$$9.99.

       01  PRINT-SUM.
           05 PRN-US-TOTAL-SALE   PIC    $,$$$,$$$,$$9.99.
           05 PRN-US-BRANCH-COUNT  PIC   ZZZ,ZZ9.
           05 PRN-US-AVERAGE-SALES  PIC   $$$,$$$,$$9.99.    


      *    **US-TOTALS.
       01   US-TOTALS.
           05 US-TOTALS-SALES   PIC   9(9)V99.
           05 US-BRANCH-COUNT   PIC  9(6).


       PROCEDURE DIVISION.
           MOVE  ZEROS TO STATE-SALE-TABLE 
           OPEN  INPUT BRANCH_SALE_FILE 
           
           PERFORM READ-RECORD-FILE

           PERFORM UNTIL END-OF-FILE
               ADD   CANDY-SALES TO STATE-SALES-TOTAL(STATE-NUM)
                     ,US-TOTALS-SALES 
               ADD  1 TO STATE-BRANCH-COUNT(STATE-NUM)
                    ,US-BRANCH-COUNT 
               PERFORM READ-RECORD-FILE
           END-PERFORM

      *    PRN
           PERFORM PRINT-RESULT

           CLOSE BRANCH_SALE_FILE  
           GOBACK 
       .

       
       READ-RECORD-FILE.
           READ BRANCH_SALE_FILE
           AT END SET END-OF-FILE  TO TRUE
           END-READ 
           EXIT 
           .

       PRINT-RESULT.
           DISPLAY "   YoureCandyShop Sales by State  "
           DISPLAY "   -----------------------------    "
           DISPLAY "STATE    BRANCHS   SALES    AVERAGESALES"
              
      *       ดึงข้อมูลออกมาจาก array
           PERFORM VARYING STATE-IDX FROM 1 BY 1 UNTIL STATE-IDX>50
              MOVE STATE-IDX TO PRN-SATTE-NUM
              MOVE STATE-SALES-TOTAL(STATE-IDX) TO PRN-STATE-SALE 
              MOVE STATE-BRANCH-COUNT (STATE-IDX) TO PRN-BRANCH-COUNT 
              COMPUTE  PRN-AVERAGE-SALES =  
              STATE-SALES-TOTAL(STATE-IDX) / 
              STATE-BRANCH-COUNT(STATE-IDX) 
              DISPLAY PRINT-DETAIL-LINE 
           END-PERFORM
      *    ปริ้นผลรวม
           MOVE US-TOTALS-SALES to PRN-US-TOTAL-SALE 
           MOVE  US-BRANCH-COUNT   TO PRN-US-BRANCH-COUNT 
           COMPUTE  PRN-US-AVERAGE-SALES =
           US-TOTALS-SALES / US-BRANCH-COUNT 
           DISPLAY "YoureCandyShop branches in the US  ",
              PRN-US-BRANCH-COUNT 
           DISPLAY "SALES " PRN-US-TOTAL-SALE 
           DISPLAY "AVERAGE " PRN-US-AVERAGE-SALES 
           DISPLAY STATE-TOTAL(1)
           EXIT 
           .