       IDENTIFICATION DIVISION. 
       PROGRAM-ID. ONE-DIM.
       AUTHOR. PAKAWAT.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  WS-ROW   OCCURS 3 TIMES .
           05 WS-COL   PIC  X VALUE "-" OCCURS 3 TIMES.
       01  WS-IDX-ROW PIC 9.
           88 WS-END-ROW  VALUE 4  THRU  9. 
       01  WS-IDX-COL  PIC   9.
           88 WS-END-COL  VALUE 4  THRU  9. 
       

       PROCEDURE DIVISION .
      *    PRINT แถว
      *    PERFORM VARYING  WS-IDX-ROW FROM 1 BY 1 UNTIL WS-END-ROW 
      *       DISPLAY  WS-ROW(WS-IDX-ROW)
      *    END-PERFORM
           PERFORM VARYING  WS-IDX-ROW FROM 1 BY 1 UNTIL WS-END-ROW 
            PERFORM VARYING  WS-IDX-COL FROM 1 BY 1 UNTIL WS-END-COL 
              DISPLAY  WS-COL(WS-IDX-ROW ,  WS-IDX-COL ) 
                                                     WITH NO ADVANCING    
            END-PERFORM
            DISPLAY ""
           END-PERFORM
           GOBACK 
       .

