       IDENTIFICATION DIVISION. 
       PROGRAM-ID. OX-GAME.
       AUTHOR. PAKAWAT.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  WS-TABLE.
           05  WS-ROW   OCCURS 3 TIMES.
            10 WS-COL   PIC   X VALUE "-"  OCCURS 3 TIMES.
      *    **INDEX**
       01  WS-IDX-ROW  PIC 9.
           88 WS-IDX-ROW-END VALUE 4 THRU 9.
       01  WS-IDX-COL  PIC 9.
           88 WS-IDX-COL-END VALUE 4 THRU 9.
       01  WS-IDX   PIC   9.
       01  WS-COUNT-CHECKWIN   PIC   9.
           88 WS-HAS-WINNER VALUE "3".
       01  WS-COUNT-CHECK-ANTI-DIAG   PIC 9.
           
      *    **INPUT**
       01  WS-INPUT-ROW   PIC   9.
           88 WS-INPUT-ROW-VALID   VALUE 1  THRU 3.
       01  WS-INPUT-COL   PIC   9.
           88 WS-INPUT-COL-VALID   VALUE 1  THRU 3.
      *    ENDGAME
       01   WS-COUNT PIC   9(2).
           88 WS-END-GAME VALUE 10 THRU 99.
           88 WS-DRAW-GAME VALUE 8.
       01  WS-PLAYER   PIC  X  VALUE "X".


       PROCEDURE DIVISION .
           PERFORM  UNTIL WS-END-GAME OR WS-HAS-WINNER OR WS-DRAW-GAME
               PERFORM  DISPLAY-TURN
               PERFORM  INPUT-ROW-COL
               PERFORM  PUT-TABLE
               PERFORM DISPLAY-TABLE  
               PERFORM RESET-ROW-COL
               IF WS-HAS-WINNER
                 DISPLAY "WE HAVE WINNER THAT IS PLAYER " WS-PLAYER 
               END-IF
      *        DRAW
               IF WS-DRAW-GAME 
                 DISPLAY  "DRAW GAME !!!"
               END-IF 
              
           END-PERFORM 
           GOBACK
       .

       DISPLAY-TURN.
           DISPLAY "TURN : " WS-PLAYER            
           EXIT
           .
       
       INPUT-ROW-COL.
           PERFORM  UNTIL WS-INPUT-ROW-VALID 
           DISPLAY  "INPUT ROW - "
           ACCEPT WS-INPUT-ROW
           END-PERFORM
           PERFORM  UNTIL WS-INPUT-COL-VALID 
           DISPLAY  "INPUT COL - "
           ACCEPT WS-INPUT-COL
           END-PERFORM
           EXIT
           .

       PUT-TABLE.
           IF WS-COL(WS-INPUT-ROW,WS-INPUT-COL ) = "-"
              MOVE  WS-PLAYER TO   WS-COL(WS-INPUT-ROW,WS-INPUT-COL)
              ADD   1  TO WS-COUNT 
              PERFORM  CHECKWIN
              IF NOT WS-HAS-WINNER 
                 PERFORM TURN-PLAYER
              END-IF
           ELSE 
              DISPLAY "ERROR"
           END-IF 
           EXIT 
           .

       DISPLAY-TABLE.
           PERFORM  VARYING WS-IDX-ROW FROM 1  BY 1  
           UNTIL WS-IDX-ROW-END
              DISPLAY  WS-ROW (WS-IDX-ROW )
           END-PERFORM 
           EXIT 
           .
       TURN-PLAYER.
           IF WS-PLAYER = "X"
              MOVE "O"  TO WS-PLAYER
           ELSE 
               MOVE "X"  TO WS-PLAYER
           END-IF

           EXIT 
           .

       CHECKWIN.
           
           PERFORM CHECKROW
           PERFORM CHECKCOL
           PERFORM CHECKDIAG
           PERFORM CHECK-ANTI-DIAG

           EXIT 
           .

       CHECKROW.
           PERFORM VARYING WS-IDX FROM 1 BY 1 UNTIL WS-IDX >3
              IF WS-COL(WS-INPUT-ROW ,WS-IDX) = WS-PLAYER 
                 ADD 1 TO WS-COUNT-CHECKWIN
              END-IF
            END-PERFORM

            IF NOT WS-COUNT-CHECKWIN = 3
              MOVE 0 TO WS-COUNT-CHECKWIN
            END-IF
           EXIT.
       CHECKCOL.
           PERFORM VARYING WS-IDX FROM 1 BY 1 UNTIL WS-IDX >3
              IF WS-COL(WS-IDX ,WS-INPUT-COL ) = WS-PLAYER 
                 ADD 1 TO WS-COUNT-CHECKWIN
              END-IF
            END-PERFORM

            IF NOT WS-COUNT-CHECKWIN = 3
              MOVE 0 TO WS-COUNT-CHECKWIN
            END-IF
           EXIT.
       CHECKDIAG.
      *    (1,1) (2,2) (3,3)
           IF WS-INPUT-COL = WS-INPUT-ROW 
            PERFORM VARYING WS-IDX FROM 1 BY 1 UNTIL WS-IDX >3
               IF WS-COL(WS-IDX ,WS-IDX ) = WS-PLAYER 
                  ADD 1 TO WS-COUNT-CHECKWIN
               END-IF
             END-PERFORM
           END-IF 
            IF NOT WS-COUNT-CHECKWIN = 3
              MOVE 0 TO WS-COUNT-CHECKWIN
            END-IF
           EXIT.
       CHECK-ANTI-DIAG.
      *    (1,3) (2,2) (3,1) ผลรวมทั้งหมดได้ 4
           IF (WS-INPUT-COL + WS-INPUT-ROW) = 4
              PERFORM VARYING WS-IDX FROM 1 BY 1 UNTIL WS-IDX >3
      *          (3,2,1)
               COMPUTE WS-COUNT-CHECK-ANTI-DIAG = 4 - WS-IDX
               IF WS-COL(WS-IDX , WS-COUNT-CHECK-ANTI-DIAG ) = WS-PLAYER 
                  ADD 1 TO WS-COUNT-CHECKWIN
               END-IF
               END-PERFORM
           END-IF 
            IF NOT WS-COUNT-CHECKWIN = 3
              MOVE 0 TO WS-COUNT-CHECKWIN
            END-IF

           EXIT 
           . 
              
       

       RESET-ROW-COL.
      *    ทำให้เงื่อนไข paragraph INPUT-ROW-COL เป็นเท็จ
           MOVE  ZEROS TO WS-INPUT-ROW , WS-INPUT-COL
           EXIT 
           .