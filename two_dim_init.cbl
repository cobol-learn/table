       IDENTIFICATION DIVISION. 
       PROGRAM-ID. ONE-DIM.
       AUTHOR. PAKAWAT.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  WS-VALUE PIC   X(9)  VALUE "123456789".

      *    ** INIT  TABLE **
       01  WS-TABLE REDEFINES WS-VALUE .
           05  WS-ROW   OCCURS 3 TIMES .
              10 WS-COL PIC X   OCCURS 3 TIMES.
      *      ** COUNT **
       01  WS-IDX-ROW PIC 9.
           88 WS-END-ROW  VALUE 4  THRU  9. 
       01  WS-IDX-COL  PIC   9.
           88 WS-END-COL  VALUE 4  THRU  9. 
       

       PROCEDURE DIVISION .
           PERFORM VARYING  WS-IDX-ROW FROM 1 BY 1 UNTIL WS-END-ROW 
            PERFORM VARYING  WS-IDX-COL FROM 1 BY 1 UNTIL WS-END-COL 
              DISPLAY  WS-COL(WS-IDX-ROW ,  WS-IDX-COL )  
                                               WITH NO ADVANCING 
            END-PERFORM
           END-PERFORM
           GOBACK 
       .

